package clases;

import java.io.Serializable;
import java.util.*;

import static java.lang.Math.round;
import static java.lang.Math.sqrt;

/**
 * Clase que representa una hashtable con direccionamiento abierto
 * @version Noviembre 2018
 * @param <K> el tipo de los objetos que serán usados como clave en la tabla.
 * @param <V> el tipo de los objetos que serán los valores de la tabla.
 */
public class TSB_OAHashtable<K,V> implements Map<K,V>, Cloneable, Serializable
{
    //************************ Constantes

    // el tamaño máximo que podrá tener el arreglo de soprte...
    // Mayor primo admitido por Integer
    private final static int MAX_SIZE = 2147473;

    // tamaño por defecto
    private final static int CAPACITY = 13;

    // Factor de carga por defecto.
    private final static float LOAD_FACTOR = 0.5f;

    //************************ Atributos privados (estructurales).

    // la tabla hash
    private Entry<K, V> table[];

    // el tamaño inicial de la tabla (tamaño con el que fue creada)...
    private int initialCapacity;

    // la cantidad de objetos que contiene la tabla en TODAS sus listas...
    private int count;

    // factor de carga para saber si hay necesidad de hacer rehashing
    // no debe ser mayor a 0.5f
    private float loadFactor;
    
    //************************ Atributos privados (para gestionar las vistas).

    /*
     * (Tal cual están definidos en la clase java.util.Hashtable)
     * Cada uno de estos campos se inicializa para contener una instancia de la
     * vista que sea más apropiada, la primera vez que esa vista es requerida. 
     * La vista son objetos stateless (no se requiere que almacenen datos, sino 
     * que sólo soportan operaciones), y por lo tanto no es necesario crear más 
     * de una de cada una.
     */
    private transient Set<K> keySet = null;
    private transient Set<Map.Entry<K,V>> entrySet = null;
    private transient Collection<V> values = null;

    
    //************************ Atributos protegidos (control de iteración).
    
    // conteo de operaciones de cambio de tamaño (fail-fast iterator).
    protected transient int modCount;
    
    
    //************************ Constructores.

    public TSB_OAHashtable()
    {
        this(10, LOAD_FACTOR);
    }
    
    /**
     * Crea una tabla vacía, con la capacidad inicial indicada y con factor 
     * de carga igual a 0.5f.
     * @param initial_capacity la capacidad inicial de la tabla.
     */    
    public TSB_OAHashtable(int initial_capacity)
    {
        this(initial_capacity, LOAD_FACTOR);
    }

    /**
     * Crea una tabla vacía, con la capacidad inicial indicada y con el factor 
     * de carga indicado. La capacidad inicial debe ser numero primo,
     * El factor de carga tiene que ser menor a 0.5f.
     * @param initial_capacity la capacidad inicial de la tabla.
     * @param load_factor el factor de carga de la tabla.
     */
    public TSB_OAHashtable(int initial_capacity, float load_factor)
    {
        // TODO: Validaciones indicadas en el comentario de arriba

        setLoadFactor(load_factor);
        setInitialCapacity(initial_capacity); // Debe estar despues de setLoadFactor();
        this.table = new Entry[initial_capacity];
        this.count = 0;
        this.modCount = 0;
    }
    
    /**
     * Crea una tabla a partir del contenido del Map especificado.
     * @param t el Map a partir del cual se creará la tabla.
     */     
    public TSB_OAHashtable(Map<? extends K,? extends V> t)
    {
        this((t.size() * 2) + 1, LOAD_FACTOR);
        this.putAll(t);
    }

    private void setLoadFactor(float load_factor)
    {
        // la asignacion de nombres mas horrible de mi vida
        // load_factor parametro
        // loadFactor atributo
        // LOAD_FACTOR constante

        if (load_factor <= 0 || load_factor > 0.5f){
            load_factor = LOAD_FACTOR;
        }
        loadFactor = load_factor;
    }

    public void setInitialCapacity(int initial_capacity)
    {
        if(initial_capacity <= 0){
            initial_capacity = CAPACITY;
        } else if (initial_capacity > TSB_OAHashtable.MAX_SIZE) {
            initial_capacity = TSB_OAHashtable.MAX_SIZE;
        } else if (!esPrimo(initial_capacity)) {
                initial_capacity = siguientePrimo(initial_capacity);
        }


        this.initialCapacity = initial_capacity;
    }

    //************************ Implementación de métodos especificados por Map.
    
    /**
     * Retorna la cantidad de elementos contenidos en la tabla.
     * @return la cantidad de elementos de la tabla.
     */
    @Override
    public int size() 
    {
        return this.count;
    }

    /**
     * Determina si la tabla está vacía (no contiene ningún elemento).
     * @return true si la tabla está vacía.
     */
    @Override
    public boolean isEmpty() 
    {
        return (this.count == 0);
    }

    /**
     * Determina si la clave key está en la tabla. 
     * @param key la clave a verificar.
     * @return true si la clave está en la tabla.
     * @throws NullPointerException si la clave es null.
     */
    @Override
    public boolean containsKey(Object key) 
    {
        if (key == null) {
            throw new NullPointerException("key nula");
        }

        return (this.get((K)key) != null);
    }

    /**
     * Determina si alguna clave de la tabla está asociada al objeto value que
     * entra como parámetro. Equivale a contains().
     * @param value el objeto a buscar en la tabla.
     * @return true si alguna clave está asociada efectivamente a ese value.
     */    
    @Override
    public boolean containsValue(Object value)
    {
        if (value == null) {
            throw new NullPointerException("valor nulo");
        }
        for (Entry entrada : this.table){
            if (entrada != null){
                if (entrada.getEstado() == 1 && value.equals(entrada.value)){
                    return true;}
            }
        }
        return false;
    }

    /**
     * Retorna el objeto al cual está asociada la clave key en la tabla, o null 
     * si la tabla no contiene ningún objeto asociado a esa clave.
     * @param key la clave que será buscada en la tabla.
     * @return el objeto asociado a la clave especificada (si existe la clave) o 
     *         null (si no existe la clave en esta tabla).
     * @throws NullPointerException si key es null.
     * @throws ClassCastException si la clase de key no es compatible con la 
     *         tabla.
     */
    @Override
    public V get(Object key)
    {
        if(key == null) throw new NullPointerException("key nula");

        Entry<K, V> entrada = search_for_entry((K) key);
        V value = null;

        if (entrada != null) { value = entrada.getValue();}
        return value;
    }

    /**
     * Asocia el valor (value) especificado, con la clave (key) especificada en
     * esta tabla. Si la tabla contenía previamente un valor asociado para la 
     * clave, entonces el valor anterior es reemplazado por el nuevo (y en este 
     * caso el tamaño de la tabla no cambia). 
     * @param key la clave del objeto que se quiere agregar a la tabla.
     * @param value el objeto que se quiere agregar a la tabla.
     * @return el objeto anteriormente asociado a la clave si la clave ya 
     *         estaba asociada con alguno, o null si la clave no estaba antes 
     *         asociada a ningún objeto.
     * @throws NullPointerException si key es null o value es null.
     */
    @Override
    public V put(K key, V value) {
        if (key == null || value == null) throw new NullPointerException("put(): parámetro null");

        Entry<K, V> entrada = search_for_entry((K) key);
        V viejo = null;

        if (entrada != null) {
            viejo = entrada.setValue(value);
        } else {
            if (this.size() > (round(this.table.length * loadFactor))) {this.rehash();}

                Entry<K, V> nuevo = new Entry<>(key, value);
                int newEntryIndex = this.getNewEntryIndex(key);
                this.table[newEntryIndex] = nuevo;
                this.count++;
                this.modCount++;
        }
        return viejo;
    }

    /**
     * Elimina de la tabla la clave key (y su correspondiente valor asociado).  
     * El método no hace nada si la clave no está en la tabla. 
     * @param key la clave a eliminar.
     * @return El objeto al cual la clave estaba asociada, o null si la clave no
     *         estaba en la tabla.
     * @throws NullPointerException - if the key is null.
     */
    @Override
    public V remove(Object key) 
    {
       if(key == null) throw new NullPointerException("remove(): parámetro null");

       Entry<K, V> entrada = search_for_entry((K) key);
       V viejo = null;

       if(entrada != null)
       {
           entrada.setEstado(0);
           this.count--;
           this.modCount++;
           viejo = entrada.getValue();
       }
       return viejo;
    }

    /**
     * Copia en esta tabla, todos los objetos contenidos en el map especificado.
     * Los nuevos objetos reemplazarán a los que ya existan en la tabla 
     * asociados a las mismas claves (si se repitiese alguna).
     * @param m el map cuyos objetos serán copiados en esta tabla. 
     * @throws NullPointerException si m es null.
     */
    @Override
    public void putAll(Map<? extends K, ? extends V> m) 
    {
        for(Map.Entry<? extends K, ? extends V> e : m.entrySet())
        {
            put(e.getKey(), e.getValue());
        }
    }

    /**
     * Elimina todo el contenido de la tabla, de forma de dejarla vacía. En esta
     * implementación además, el arreglo de soporte vuelve a tener el tamaño que
     * inicialmente tuvo al ser creado el objeto.
     */
    @Override
    public void clear() 
    {
        this.table = new Entry[initialCapacity];
        this.count = 0;
        this.modCount++;
    }

    /**
     * Retorna un Set (conjunto) a modo de vista de todas las claves (key)
     * contenidas en la tabla. El conjunto está respaldado por la tabla, por lo 
     * que los cambios realizados en la tabla serán reflejados en el conjunto, y
     * viceversa. Si la tabla es modificada mientras un iterador está actuando 
     * sobre el conjunto vista, el resultado de la iteración será indefinido 
     * (salvo que la modificación sea realizada por la operación remove() propia
     * del iterador, o por la operación setValue() realizada sobre una entrada 
     * de la tabla que haya sido retornada por el iterador). El conjunto vista 
     * provee métodos para eliminar elementos, y esos métodos a su vez 
     * eliminan el correspondiente par (key, value) de la tabla (a través de las
     * operaciones Iterator.remove(), Set.remove(), removeAll(), retainAll() 
     * y clear()). El conjunto vista no soporta las operaciones add() y 
     * addAll() (si se las invoca, se lanzará una UnsuportedOperationException).
     * @return un conjunto (un Set) a modo de vista de todas las claves
     *         mapeadas en la tabla.
     */
    @Override
    public Set<K> keySet() 
    {
        if(keySet == null) 
        { 
            // keySet = Collections.synchronizedSet(new KeySet()); 
            keySet = new KeySet();
        }
        return keySet;  
    }
        
    /**
     * Retorna una Collection (colección) a modo de vista de todos los valores
     * (values) contenidos en la tabla. La colección está respaldada por la 
     * tabla, por lo que los cambios realizados en la tabla serán reflejados en 
     * la colección, y viceversa. Si la tabla es modificada mientras un iterador 
     * está actuando sobre la colección vista, el resultado de la iteración será 
     * indefinido (salvo que la modificación sea realizada por la operación 
     * remove() propia del iterador, o por la operación setValue() realizada 
     * sobre una entrada de la tabla que haya sido retornada por el iterador). 
     * La colección vista provee métodos para eliminar elementos, y esos métodos 
     * a su vez eliminan el correspondiente par (key, value) de la tabla (a 
     * través de las operaciones Iterator.remove(), Collection.remove(), 
     * removeAll(), removeAll(), retainAll() y clear()). La colección vista no 
     * soporta las operaciones add() y addAll() (si se las invoca, se lanzará 
     * una UnsuportedOperationException).
     * @return una colección (un Collection) a modo de vista de todas los 
     *         valores mapeados en la tabla.
     */
    @Override
    public Collection<V> values() 
    {
        if(values==null)
        {
            // values = Collections.synchronizedCollection(new ValueCollection());
            values = new ValueCollection();
        }
        return values;    
    }

    /**
     * Retorna un Set (conjunto) a modo de vista de todos los pares (key, value)
     * contenidos en la tabla. El conjunto está respaldado por la tabla, por lo 
     * que los cambios realizados en la tabla serán reflejados en el conjunto, y
     * viceversa. Si la tabla es modificada mientras un iterador está actuando 
     * sobre el conjunto vista, el resultado de la iteración será indefinido 
     * (salvo que la modificación sea realizada por la operación remove() propia
     * del iterador, o por la operación setValue() realizada sobre una entrada 
     * de la tabla que haya sido retornada por el iterador). El conjunto vista 
     * provee métodos para eliminar elementos, y esos métodos a su vez 
     * eliminan el correspondiente par (key, value) de la tabla (a través de las
     * operaciones Iterator.remove(), Set.remove(), removeAll(), retainAll() 
     * and clear()). El conjunto vista no soporta las operaciones add() y 
     * addAll() (si se las invoca, se lanzará una UnsuportedOperationException).
     * @return un conjunto (un Set) a modo de vista de todos los objetos 
     *         mapeados en la tabla.
     */
    @Override
    public Set<Map.Entry<K, V>> entrySet() 
    {
        if(entrySet == null) 
        { 
            // entrySet = Collections.synchronizedSet(new EntrySet()); 
            entrySet = new EntrySet();
        }
        return entrySet;
    }

    
    //************************ Redefinición de métodos heredados desde Object.
    
    /**
     * Retorna una copia superficial de la tabla. Las listas de desborde o 
     * buckets que conforman la tabla se clonan ellas mismas, pero no se clonan 
     * los objetos que esas listas contienen: en cada bucket de la tabla se 
     * almacenan las direcciones de los mismos objetos que contiene la original. 
     * @return una copia superficial de la tabla.
     * @throws CloneNotSupportedException si la clase no implementa la
     *         interface Cloneable.    
     */ 
    @Override
    protected Object clone() throws CloneNotSupportedException 
    {
        TSB_OAHashtable<K, V> t = (TSB_OAHashtable<K, V>)super.clone();
        t.table = new Entry[table.length];
        for (int i = 0; i < this.table.length; i++)
        {
            t.table[i] = this.table[i];
        }
        t.keySet = null;
        t.entrySet = null;
        t.values = null;
        t.modCount = 0;
        return t;
    }

    /**
     * Determina si esta tabla es igual al objeto espeficicado.
     * @param obj el objeto a comparar con esta tabla.
     * @return true si los objetos son iguales.
     */
    @Override
    public boolean equals(Object obj) 
    {
        if(!(obj instanceof Map)) { return false; }
        
        Map<K, V> t = (Map<K, V>) obj;
        if(t.size() != this.size()) { return false; }

        try 
        {
            Iterator<Map.Entry<K,V>> i = this.entrySet().iterator();
            while(i.hasNext()) 
            {
                Map.Entry<K, V> e = i.next();
                K key = e.getKey();
                V value = e.getValue();
                if(t.get(key) == null) { return false; }
                else 
                {
                    if(!value.equals(t.get(key))) { return false; }
                }
            }
        } 
        
        catch (ClassCastException | NullPointerException e) 
        {
            return false;
        }

        return true;    
    }

    /**
     * Retorna un hash code para la tabla completa.
     * @return un hash code para la tabla.
     */
    @Override
    public int hashCode() 
    {
        if(this.isEmpty()) {return 0;}
        
        int hc = 0;
        for(Map.Entry<K, V> entry : this.entrySet())
        {
            hc += entry.hashCode();
        }
        
        return hc;
    }
    
    /**
     * Devuelve el contenido de la tabla en forma de String. Sólo por razones de
     * didáctica, se hace referencia explícita en esa cadena al contenido de 
     * cada una de las listas de desborde o buckets de la tabla.
     * @return una cadena con el contenido completo de la tabla.
     */
    @Override
    public String toString() 
    {
        StringBuilder cad = new StringBuilder("");
        for(Entry<K, V> entrada : this.table)
        {
            if (entrada != null && entrada.getEstado() == 1){
                cad.append(entrada.toString()).append(":\n\t");}

        }
        return cad.toString();
    }
    
    
    //************************ Métodos específicos de la clase.
    
    /**
     * Incrementa el tamaño de la tabla y reorganiza su contenido. Se invoca 
     * automaticamente cuando se detecta que la cantidad promedio de nodos por 
     * lista supera a cierto el valor critico dado por (10 * load_factor). Si el
     * valor de load_factor es 0.8, esto implica que el límite antes de invocar 
     * rehash es de 8 nodos por lista en promedio, aunque seria aceptable hasta 
     * unos 10 nodos por lista.
     */
    protected void rehash()
    {
        int old_length = this.table.length;
        
        // nuevo tamaño: doble del anterior, más uno para llevarlo a impar...
        int new_length = siguientePrimo(old_length * 2 + 1);
        
        // no permitir que la tabla tenga un tamaño mayor al límite máximo...
        // ... para evitar overflow y/o desborde de índices...
        if(new_length > TSB_OAHashtable.MAX_SIZE)
        { 
            new_length = TSB_OAHashtable.MAX_SIZE;
        }


        // crear el nuevo arreglo con new_length listas vacías...
        Entry<K, V> old_table[] = this.table.clone();

        this.table = new Entry[new_length];
        this.count = 0;
        this.modCount++;

        for (Entry<K, V> entrada : old_table){
            if (entrada != null){
                this.put(entrada.key, entrada.value);
            }
        }
    }
    

    //************************ Métodos privados.
    
    /*
     * Función hash. Toma una clave entera k y calcula y retorna un índice 
     * válido para esa clave para entrar en la tabla.     
     */
    private int h(int k)
    {
        return h(k, this.table.length);
    }
    
    /*
     * Función hash. Toma un objeto key que representa una clave y calcula y 
     * retorna un índice válido para esa clave para entrar en la tabla.     
     */
    private int h(K key)
    {
        return h(key.hashCode(), this.table.length);
    }
    
    /*
     * Función hash. Toma un objeto key que representa una clave y un tamaño de 
     * tabla t, y calcula y retorna un índice válido para esa clave dedo ese
     * tamaño.     
     */
    private int h(K key, int t)
    {
        return h(key.hashCode(), t);
    }
    
    /*
     * Función hash. Toma una clave entera k y un tamaño de tabla t, y calcula y 
     * retorna un índice válido para esa clave dado ese tamaño.     
     */
    private int h(int k, int t)
    {
        if(k < 0) k *= -1;
        return k % t;        
    }
    
    /**
     * Calcula la longitud promedio de las listas de la tabla.
     * @return la longitud promedio de la listas contenidas en la tabla.
     */

    private int averageLength()
    {
        return this.count / this.table.length;
    } 
    
    /*
     * Busca en la lista bucket un objeto Entry cuya clave coincida con key.
     * Si lo encuentra, retorna ese objeto Entry. Si no lo encuentra, retorna 
     * null.
     */
    private Entry<K, V> search_for_entry(K key)
    {
        int index = h(key);
        int i = index;
        int j = 1;

        Entry<K, V> entrada = this.table[i];

        while(entrada != null && entrada.getEstado() != 0) {
            if (key.equals(this.table[i].getKey())) return this.table[i];

            i = (index + (j*j)) % this.table.length;
            j++;

            entrada = this.table[i];
        }
        return null;
    }
    
    /*
     * Busca en la lista bucket un objeto Entry cuya clave coincida con key.
     * Si lo encuentra, retorna su posicíón. Si no lo encuentra, retorna -1.
     */
    private int search_for_index(K key)
    {
        int index = h(key);
        int i = index;
        int j = 1;

        Entry<K, V> entrada = this.table[i];

        while(entrada != null && entrada.getEstado() != 0) {
            if (key.equals(this.table[i].getKey())) return i;

            i = (index + (j*j)) % this.table.length;
            j++;

            entrada = this.table[i];
        }
        return -1;
    }

    private int siguientePrimo (int  n)
    {
        if (n % 2 == 0) n++;
        for (; !esPrimo(n); n += 2) ;
        return n;
    }

    private boolean esPrimo(int n)
    {
        if (n == 1 || n % 2 == 0){
            return false;
        }

        for (int i = 3; i < (int)round(sqrt(n)) ; i += 2) {
            if (n % i == 0){
                return false;
            }
        }

        return true;
    }

    private int getNewEntryIndex(K key) {
        int newEntryIndex = h(key);
        int i = newEntryIndex;
        int j = 1;

        while (!(table[i] == null)) {
            i = (newEntryIndex + j * j) % table.length;
            j++;
        }
        return i;
    }



    //************************ Clases Internas.
    
    /*
     * Clase interna que representa los pares de objetos que se almacenan en la
     * tabla hash: son instancias de esta clase las que realmente se guardan en 
     * en cada una de las listas del arreglo table que se usa como soporte de 
     * la tabla. Lanzará una IllegalArgumentException si alguno de los dos 
     * parámetros es null.
     */
    private class Entry<K, V> implements Map.Entry<K, V>// TODO: Iterador
    {
        private K key;
        private V value;
        private int estado; // tumba = 0, cerrado = 1, abierto = null
        
        public Entry(K key, V value) 
        {
            if(key == null || value == null)
            {
                throw new IllegalArgumentException("Entry(): parámetro null...");
            }
            this.key = key;
            this.value = value;
            this.estado = 1;
        }
        
        @Override
        public K getKey() 
        {
            return key;
        }

        @Override
        public V getValue() 
        {
            return value;
        }

        @Override
        public V setValue(V value) 
        {
            if(value == null) 
            {
                throw new IllegalArgumentException("setValue(): parámetro null...");
            }
                
            V old = this.value;
            this.value = value;
            return old;
        }

        public int getEstado() { return estado;}

        public void setEstado(int est) { if (est == 0 || est == 1) estado = est;}
       
        @Override
        public int hashCode() 
        {
            int hash = 7;
            hash = 61 * hash + Objects.hashCode(this.key);
            hash = 61 * hash + Objects.hashCode(this.value);            
            return hash;
        }

        @Override
        public boolean equals(Object obj) 
        {
            if (this == obj) { return true; }
            if (obj == null) { return false; }
            if (this.getClass() != obj.getClass()) { return false; }
            
            final Entry other = (Entry) obj;
            if (!Objects.equals(this.key, other.key)) { return false; }
            if (!Objects.equals(this.value, other.value)) { return false; }            
            return true;
        }       
        
        @Override
        public String toString()
        {
            return "(" + key.toString() + ", " + value.toString() + ")";
        }
    }
    
    /*
     * Clase interna que representa una vista de todas los Claves mapeadas en la
     * tabla: si la vista cambia, cambia también la tabla que le da respaldo, y
     * viceversa. La vista es stateless: no mantiene estado alguno (es decir, no 
     * contiene datos ella misma, sino que accede y gestiona directamente datos
     * de otra fuente), por lo que no tiene atributos y sus métodos gestionan en
     * forma directa el contenido de la tabla. Están soportados los metodos para
     * eliminar un objeto (remove()), eliminar todo el contenido (clear) y la  
     * creación de un Iterator (que incluye el método Iterator.remove()).
     */    
    private class KeySet extends AbstractSet<K> 
    {
        @Override
        public Iterator<K> iterator() 
        {
            return new KeySetIterator();
        }
        
        @Override
        public int size() 
        {
            return TSB_OAHashtable.this.size();
        }
        
        @Override
        public boolean contains(Object o) 
        {
            return TSB_OAHashtable.this.containsKey(o);
        }
        
        @Override
        public boolean remove(Object o) 
        {
            return (TSB_OAHashtable.this.remove(o) != null);
        }
        
        @Override
        public void clear() 
        {
            TSB_OAHashtable.this.clear();
        }
        
        private class KeySetIterator implements Iterator<K>
        {
            // índice del elemento actual en el iterador (el que fue retornado 
            // la última vez por next() y será eliminado por remove())...
            private int current_entry;
                        
            // flag para controlar si remove() está bien invocado...
            private boolean next_ok;
            
            // el valor que debería tener el modCount de la tabla completa...
            private int expected_modCount;
            
            /*
             * Crea un iterador comenzando en la primera lista. Activa el 
             * mecanismo fail-fast.
             */
            public KeySetIterator()
            {
                current_entry = -1;
                next_ok = false;
                expected_modCount = TSB_OAHashtable.this.modCount;
            }

            /*
             * Determina si hay al menos un elemento en la tabla que no haya 
             * sido retornado por next(). 
             */
            @Override
            public boolean hasNext() 
            {
                // variable auxiliar t para simplificar accesos...
                Entry<K, V> t[] = TSB_OAHashtable.this.table;

                if(TSB_OAHashtable.this.isEmpty()) { return false; }
                if(current_entry >= t.length - 1) { return false; }

                int next_entry = current_entry + 1;
                while(next_entry < t.length && (t[next_entry] == null || t[next_entry].getEstado() == 0))
                {
                    next_entry++;
                }
                if(next_entry >= t.length) { return false; }

                // en principio alcanza con esto... revisar...    
                return true;
            }

            /*
             * Retorna el siguiente elemento disponible en la tabla.
             */
            @Override
            public K next() {
                if (modCount != expected_modCount) throw new ConcurrentModificationException("next(): modificacion no esperada");

                if (!hasNext()) throw new NoSuchElementException("next(): no hay mas elementos");

                Entry<K, V> t[] = TSB_OAHashtable.this.table;

                current_entry++;
                while (t[current_entry] == null || t[current_entry].getEstado() == 0) current_entry++;

                next_ok = true;
                return t[current_entry].getKey();
            }

            
            /*
             * Remueve el elemento actual de la tabla, dejando el iterador en la
             * posición anterior al que fue removido. El elemento removido es el
             * que fue retornado la última vez que se invocó a next(). El método
             * sólo puede ser invocado una vez por cada invocación a next().
             */
            @Override
            public void remove() 
            {
                if(!next_ok) 
                { 
                    throw new IllegalStateException("remove(): debe invocar a next() antes de remove()..."); 
                }
                
                // eliminar el objeto que retornó next() la última vez...
                V garbage = TSB_OAHashtable.this.remove(current_entry);

                Entry<K, V> t[] = TSB_OAHashtable.this.table;

                current_entry--;

                while (t[current_entry] == null || t[current_entry].getEstado() == 0) current_entry--;
                next_ok = false;

                TSB_OAHashtable.this.count--;
                TSB_OAHashtable.this.modCount++;
                expected_modCount++;
            }     
        }
    }

    /*
     * Clase interna que representa una vista de todos los PARES mapeados en la
     * tabla: si la vista cambia, cambia también la tabla que le da respaldo, y
     * viceversa. La vista es stateless: no mantiene estado alguno (es decir, no 
     * contiene datos ella misma, sino que accede y gestiona directamente datos
     * de otra fuente), por lo que no tiene atributos y sus métodos gestionan en
     * forma directa el contenido de la tabla. Están soportados los metodos para
     * eliminar un objeto (remove()), eliminar todo el contenido (clear) y la  
     * creación de un Iterator (que incluye el método Iterator.remove()).
     */    
    private class EntrySet extends AbstractSet<Map.Entry<K, V>> 
    {

        @Override
        public Iterator<Map.Entry<K, V>> iterator() 
        {
            return new EntrySetIterator();
        }

        @Override
        public boolean contains(Object o)
        {
            if(o == null) return false;
            if(!(o instanceof Entry)) return false;

            Entry<K,V> entrada = (Entry<K,V>)o;
            K key = entrada.getKey();
            V value = entrada.getValue();

            if(TSB_OAHashtable.this.containsKey(key) && TSB_OAHashtable.this.containsValue(value)) return true;

            return false;
        }

        @Override
        public boolean remove(Object o) {
            if (!contains(o)) return false;

            Entry<K,V> entrada = (Entry<K,V>)o;
            K key = entrada.getKey();

            V garbage = TSB_OAHashtable.this.remove(key);

            TSB_OAHashtable.this.count--;
            TSB_OAHashtable.this.modCount++;
            return true;
        }

        @Override
        public int size() 
        {
            return TSB_OAHashtable.this.size();
        }

        @Override
        public void clear() 
        {
            TSB_OAHashtable.this.clear();
        }
        
        private class EntrySetIterator implements Iterator<Map.Entry<K, V>>
        {

            private int current_entry;

            // flag para controlar si remove() está bien invocado...
            private boolean next_ok;

            // el valor que debería tener el modCount de la tabla completa...
            private int expected_modCount;

            /*
             * Crea un iterador comenzando en la primera lista. Activa el
             * mecanismo fail-fast.
             */
            public EntrySetIterator()
            {
                current_entry = -1;
                next_ok = false;
                expected_modCount = TSB_OAHashtable.this.modCount;
            }

            /*
             * Determina si hay al menos un elemento en la tabla que no haya
             * sido retornado por next().
             */
            @Override
            public boolean hasNext()
            {
                // variable auxiliar t para simplificar accesos...
                Entry<K, V> t[] = TSB_OAHashtable.this.table;

                if(TSB_OAHashtable.this.isEmpty()) { return false; }
                if(current_entry >= t.length - 1) { return false; }

                int next_entry = current_entry + 1;
                while (next_entry < t.length && (t[next_entry] == null || t[next_entry].getEstado() == 0)) {
                    next_entry++;
                }
                if (next_entry >= t.length) return false;

                return true;
            }

            /*
             * Retorna el siguiente elemento disponible en la tabla.
             */
            @Override
            public Entry<K, V> next()
            {
                // control: fail-fast iterator...
                if(TSB_OAHashtable.this.modCount != expected_modCount)
                {
                    throw new ConcurrentModificationException("next(): modificación inesperada de tabla...");
                }

                if(!hasNext())
                {
                    throw new NoSuchElementException("next(): no existe el elemento pedido...");
                }

                Entry<K, V> t[] = TSB_OAHashtable.this.table;

                current_entry++;
                while (t[current_entry] == null || t[current_entry].getEstado()==0) current_entry++;
                next_ok = true;

                return t[current_entry];
            }

            /*
             * Remueve el elemento actual de la tabla, dejando el iterador en la
             * posición anterior al que fue removido. El elemento removido es el
             * que fue retornado la última vez que se invocó a next(). El método
             * sólo puede ser invocado una vez por cada invocación a next().
             */
            @Override
            public void remove()
            {
                if(!next_ok)
                {
                    throw new IllegalStateException("remove(): debe invocar a next() antes de remove()...");
                }

                // eliminar el objeto que retornó next() la última vez...
                V garbage = TSB_OAHashtable.this.remove(current_entry);

                Entry<K, V> t[] = TSB_OAHashtable.this.table;

                current_entry--;

                while (t[current_entry] == null || t[current_entry].getEstado() == 0) current_entry--;
                next_ok = false;

                TSB_OAHashtable.this.count--;
                TSB_OAHashtable.this.modCount++;
                expected_modCount++;
            }
        }
    }    
    
    /*
     * Clase interna que representa una vista de todos los VALORES mapeados en 
     * la tabla: si la vista cambia, cambia también la tabla que le da respaldo, 
     * y viceversa. La vista es stateless: no mantiene estado alguno (es decir, 
     * no contiene datos ella misma, sino que accede y gestiona directamente los
     * de otra fuente), por lo que no tiene atributos y sus métodos gestionan en
     * forma directa el contenido de la tabla. Están soportados los metodos para
     * eliminar un objeto (remove()), eliminar todo el contenido (clear) y la  
     * creación de un Iterator (que incluye el método Iterator.remove()).
     */ 
    private class ValueCollection extends AbstractCollection<V> 
    {
        @Override
        public Iterator<V> iterator() 
        {
            return new ValueCollectionIterator();
        }
        
        @Override
        public int size() 
        {
            return TSB_OAHashtable.this.size();
        }
        
        @Override
        public boolean contains(Object o) 
        {
            return TSB_OAHashtable.this.containsValue(o);
        }
        
        @Override
        public void clear() 
        {
            TSB_OAHashtable.this.clear();
        }
        
        private class ValueCollectionIterator implements Iterator<V>
        {

            private int current_entry;
                        
            // flag para controlar si remove() está bien invocado...
            private boolean next_ok;
            
            // el valor que debería tener el modCount de la tabla completa...
            private int expected_modCount;
            
            /*
             * Crea un iterador comenzando en la primera lista. Activa el 
             * mecanismo fail-fast.
             */
            public ValueCollectionIterator()
            {
                current_entry = -1;
                next_ok = false;
                expected_modCount = TSB_OAHashtable.this.modCount;
            }

            /*
             * Determina si hay al menos un elemento en la tabla que no haya 
             * sido retornado por next(). 
             */
            @Override
            public boolean hasNext() 
            {
                // variable auxiliar t para simplificar accesos...
                Entry<K, V> t[] = TSB_OAHashtable.this.table;

                if(TSB_OAHashtable.this.isEmpty()) { return false; }
                if(current_entry >= t.length -1) { return false; }
                
                int next_entry = current_entry + 1;
                while (next_entry < t.length && (t[next_entry] == null || t[next_entry].getEstado() == 0)) {
                    next_entry++;
                }
                if (next_entry >= t.length) return false;

                return true;
            }

            /*
             * Retorna el siguiente elemento disponible en la tabla.
             */
            @Override
            public V next() 
            {
                // control: fail-fast iterator...
                if(TSB_OAHashtable.this.modCount != expected_modCount)
                {    
                    throw new ConcurrentModificationException("next(): modificación inesperada de tabla...");
                }
                
                if(!hasNext()) 
                {
                    throw new NoSuchElementException("next(): no existe el elemento pedido...");
                }

                Entry<K, V> t[] = TSB_OAHashtable.this.table;
                
                current_entry++;
                while (t[current_entry] == null || t[current_entry].getEstado()==0) current_entry++;
                next_ok = true;

                return t[current_entry].getValue();
            }
            
            /*
             * Remueve el elemento actual de la tabla, dejando el iterador en la
             * posición anterior al que fue removido. El elemento removido es el
             * que fue retornado la última vez que se invocó a next(). El método
             * sólo puede ser invocado una vez por cada invocación a next().
             */
            @Override
            public void remove()
            {
                if(!next_ok)
                {
                    throw new IllegalStateException("remove(): debe invocar a next() antes de remove()...");
                }

                // eliminar el objeto que retornó next() la última vez...
                V garbage = TSB_OAHashtable.this.remove(current_entry);

                Entry<K, V> t[] = TSB_OAHashtable.this.table;

                current_entry--;

                while (t[current_entry] == null || t[current_entry].getEstado() == 0) current_entry--;
                next_ok = false;

                TSB_OAHashtable.this.count--;
                TSB_OAHashtable.this.modCount++;
                expected_modCount++;
            }
        }
    }
}
