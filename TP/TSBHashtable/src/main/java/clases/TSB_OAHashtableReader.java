package clases;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class TSB_OAHashtableReader {
    private String file = "HashTablePalabras.dat";

    public TSB_OAHashtableReader(){}

    public TSB_OAHashtableReader(String name)
    {
        file = name;
    }

    public TSB_OAHashtable read() throws TSB_OAHashtableIOException
    {
        TSB_OAHashtable table = null;

        try
        {
            FileInputStream stream = new FileInputStream(file);
            ObjectInputStream obj = new ObjectInputStream(stream);

            table = (TSB_OAHashtable) obj.readObject();
            System.out.println("Archivo leido");
            obj.close();
            stream.close();
        }
        catch (IOException ex)
        {
            throw new TSB_OAHashtableIOException("No se puedo recuperar datos de HashTable");
        }

        return table;
    }
}
