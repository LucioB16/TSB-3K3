package clases;

import java.util.Map;

public class HashTablePalabras {

    public TSB_OAHashtable<String, Integer> table;

    public HashTablePalabras()
    {
        this(13);
    }

    public HashTablePalabras(int initialCapacity)
    {
        this.table = new TSB_OAHashtable<>(initialCapacity);
    }

    public HashTablePalabras(Map<? extends String, ? extends Integer> t)
    {
        this.table = new TSB_OAHashtable<>(t);
    }

    public int add(int repeticiones, String palabra)
    {
        int viejo;
        if (this.table.containsKey(palabra)){
            viejo = this.table.put(palabra, this.table.get(palabra) + repeticiones);
        }
        else{
            this.table.put(palabra, repeticiones);
            viejo = 1;
        }
        return viejo;
    }

    public int remove(String palabra)
    {
        return  this.table.remove(palabra);
    }

    public boolean isEmpty()
    {
        return this.table.isEmpty();
    }

    public int getRepeticiones(String palabra)
    {
        return this.table.get(palabra);
    }

    public String toString()
    {
        return this.table.toString();
    }

    public void write(){
        try
        {
            TSB_OAHashtableWriter grabador = new TSB_OAHashtableWriter();
            grabador.write(this.table);
        }
        catch (TSB_OAHashtableIOException ex)
        {
            System.out.println("ERROR: " + ex.getMessage());
        }
    }

    public void read(){
        try
        {
            TSB_OAHashtableWriter lector = new TSB_OAHashtableReader();
            this.table = (TSB_OAHashtable<String, Integer>) lector.read();
        }
        catch (TSB_OAHashtableIOException ex)
        {
            System.out.println("ERROR: " + ex.getMessage());
        }
    }

}
